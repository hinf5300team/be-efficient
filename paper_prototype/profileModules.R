profileUI <- function(id, user_profile, defaults){
  list2env(defaults, env = environment())
  ns <- NS(id)
 fluidPage(
  tags$head(tags$style(".shiny-input-container:not(.shiny-input-container-inline) {width:100%;}")),
  htmlOutput(ns("intention")),
  textOutput(ns("warn")),
  fluidRow(width = 12, class = "panel panel-default",
           column(4,
                  textInput(inputId = ns("rt_key"),
                            label = "Rescuetime API key:", width = "250px", value = user_profile$rt_key)),
           column(4,
                  uiOutput(ns("goalsUI"))),
           column(4,
                  radioButtons(ns("parent"), "Are you a parent or caretaker?", choices = list(Yes = T, No = F), selected = user_profile$parent))
  ),
  fluidRow(width = 12, class = "panel panel-default",
           div(class = "panel-heading", "Personal Information"),
           div(class = "panel-body",
               column(3,
                      textInput(inputId = ns("fn"),
                                label = "First Name", value = user_profile$fn)
               ),
               column(3,
                      textInput(inputId = ns("ln"),
                                label = "Last Name", value = user_profile$ln)
                      ),
               column(3,
                      textInput(inputId = ns("email"),
                                label = "Email Address", value = user_profile$email)
               ),
               column(3,
                      textInput(inputId = ns("pn"),
                                label = "Phone Number", value = user_profile$pn)
               )
           )
  ),
  fluidRow(class = "panel panel-default", 
           div(class = "panel-heading", "Supportive Accountability Buddies"),
           div(class = "panel-body", 
               column(12,
                      div(class = "wrap-input center-block text-center", style = "width:100%;", 
                          actionButton(ns("addBuddy"), "Invite more", icon = icon("plus-circle", lib = "font-awesome")),
                          div(id = ns("buds"),
                              uiOutput(ns("ebUI"))
                          ),
                          textAreaInput(ns("b_m"), "Message", width = "100%", height = "150px", resize = "both", value = "Hi Friend, I'm using Be-Efficient to achieve my healthy lifestyle intentions. Will you be a supportive accountability buddy for me?"),
                          fluidRow(
                            column(12,
                                   div(class = "center-block", 
                                       actionButton(ns("inviteBuddy"), "Send Invitation(s)", icon = icon("envelope", lib = "font-awesome"))
                                   )
                            )
                          )
                      )
               )
           )
  ), 
  fluidRow(width = 12, class = "center-block text-center panel panel-default",
           div(class = "panel-heading", "Interventions"),
           div(class = "panel-body",
               column(6, 
                      div(class = "wrap-input center-block text-center", id = ns("int_sms"), style = "width:100%;",
                          tags$h4("Just-in-time (SMS)"),
                          p(class = "panel-text", "Configure the times of the day and maximum intervention text messages you’re open to receiving during the day below."),
                          p(class = "panel-text", style = "font-size:.8em", "The default is three interventions max on each day of the week."), checkboxInput(ns("enSMS"), label = "Enable Just-in-time SMS", value = user_profile$enSMS),
                          actionButton(ns("addSMS"), "Add Window", icon = icon("plus-circle", lib = "font-awesome")),
                          uiOutput(ns("smsUI"))
                      )
               ), # End of first column with SMS config
               column(6, 
                      div(class = "wrap-input center-block text-center", id = ns("int_email"), style = "width:100%;",
                          tags$h4("Interval (Email)"),
                          p(class = "panel-text", "If you want updates via email, you can select the time(s) and day(s) you would like those emails delivered below."),
                          p(class = "panel-text", style = "font-size:.8em", "The default is 8am every weekday."), checkboxInput(ns("enEmail"), label = "Enable Interval Emails", value = user_profile$enEmail),
                          actionButton(ns("addEmail"), "Add Time", icon = icon("plus-circle", lib = "font-awesome")),
                          uiOutput(ns("emailUI"))
                      )
               )
           )
  ),
  fluidRow(width = 12, class = "center-block text-center", actionButton(ns("save"), "Save Profile", icon("save")))
  #, shiny::htmlOutput(ns("test.text"))
  
 )
}

profileSV <- function(input, output, session, user_profile, defaults) {
  list2env(defaults, env = environment())
  source("introModules.R")
  output$intention <- renderUI({tags$h3("Intention(s): ", tags$br(), tags$blockquote(user_profile$intention))})
  # # Render profile values for debug
  # output$test.text <- shiny::renderPrint({
  #   names(user_profile) %>% purrr::map(~{
  #     paste0(.x,": ",user_profile[[.x]], collapse = " ")
  #   }) %>% paste0(collapse = "<br/>")
  # })
  # Update Text entry fields
  observeEvent(purrr::map( c("rt_key", "fn", "ln", "pn", "email"), ~{purrr::pluck(user_profile, .x)}), {
    c("rt_key", "fn", "ln", "pn", "email") %>% purrr::walk(~ {
      if (!is.null(user_profile[[.x]]) & .x != "pn") {
        updateTextInput(session, .x, value = user_profile[[.x]])
      } else {
        updateTextInput(session, .x, value = as.character(user_profile[[.x]]["pn"]))
      }
    })
  })
  
  observeEvent(user_profile$parent, {
    updateRadioButtons(session, "parent", selected = user_profile$parent)
    updateCheckboxInput(session, "enSMS", value = user_profile$enSMS)
    updateCheckboxInput(session, "enEmail", value = user_profile$enEmail)
  })
  # Save Updated Profile values ----
  
  warnText <- eventReactive(input$save, {
    
    
    
      user_profile$fn <<- input$fn
      user_profile$ln <<- input$ln
      # Email validation
      shinyFeedback::feedbackWarning(session$ns("email"), condition = {!stringr::str_detect(input$email, valid_email)}, text = "Please enter a valid email")
      validate(
        need(stringr::str_detect(input$email, valid_email), "Please enter a valid email")
      )
      user_profile$email <<- input$email
      .pn <-  stringr::str_replace_all(input$pn, "[^[0-9]+]+", "")
      shinyFeedback::feedbackWarning(session$ns("pn"), condition = {nchar(.pn) < 10}, text = "Please enter a ten digit phone number") # Create warning if invalid phone number
      validate(
        need(nchar(.pn) >= 10, message = "Please enter a ten digit phone number")
      )
      if (nchar(.pn) >= 10) { # if the phone number is a valid 10 digits
        # Carrier Lookup
        # UNCOMMENT IN LIVE
        # .url <- httr::parse_url("https://lookups.twilio.com/v1/PhoneNumbers/")
        # .url <- httr::modify_url(.url, path = paste0(.url$path,.pn))
        # .url <- httr::modify_url(.url, query = list(Type = "carrier"))
        # .resp <- httr::GET(.url,  httr::authenticate(twilio:::get_sid(), twilio:::get_token()))
        # .carrier <- httr::content(.resp)$carrier$name
        .carrier <- "Verizon Wireless"
        .mms <- stringr::str_replace(carrier_suffix[agrep(.carrier, carrier_suffix$Carrier), "Gateway", drop = T], "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")", .pn) # Get the mms gateway from carrier_suffix
        user_profile$pn <<- c(pn = .pn, mms = .mms) # Store the 10 digit pn and the email to mms gateway
      }
    
      shinyFeedback::feedbackWarning(session$ns("rt_key"), condition = {nchar(input$rt_key) != 40}, text = "Be-Efficient requires the 40 character Rescuetime API Key")
      validate(
        need(nchar(input$rt_key) == 40, "Be-Efficient requires the 40 character Rescuetime API Key")
      )
      user_profile$rt_key <<- input$rt_key
   
      # See observeEvent for inviteBuddy for validation
      purrr::walk(stringr::str_subset(names(input), "b\\d"), .input = input, function(.x, .input){
        user_profile[[.x]] <<- input[[.x]]
      })
      
      # Goals modal
      
      if (!is.null(input$goal1) & length(try(nchar(input$goal1))) > 0) {
        
        message("Goal input detected. Attempting to make into DF")
        # if there are entries for goals
        .int_goal <- purrr::map(stringr::str_extract(names(input), "(?<=goal)\\d") %>% purrr::keep(!is.na(.)) %>% as.numeric %>% setNames(nm = .), ~ {
          if (stringr::str_detect(input[[paste0("goal", .x)]], stringr::regex("m|h", ignore_case = T))) {
            message("m or h detected, coercing to duration")
            .goal <- lubridate::hm(
              paste0(
                ifelse(is.na(stringr::str_extract(input[[paste0("goal", .x)]], stringr::regex("\\d{1,2}(?=h)", ignore_case = T))),"00", stringr::str_extract(input[[paste0("goal", .x)]], stringr::regex("\\d{1,2}(?=h)", ignore_case = T)))
                ,":" ,
                ifelse(is.na(stringr::str_extract(input[[paste0("goal", .x)]], stringr::regex("\\d{1,2}(?=\\s?m)", ignore_case = T))),"00",stringr::str_extract(input[[paste0("goal", .x)]], stringr::regex("\\d{1,2}(?=\\s?m)", ignore_case = T)))
              ))
          } else if (stringr::str_detect(input[[paste0("goal", .x)]], "^\\:")) {
            message("Semi-colon detected as first char, adding 0")
            .goal <- stringr::str_replace(input[[paste0("goal", .x)]], "^", "0") %>% # if no leading 0 before the colon, add it
              lubridate::hm()
          } else {
            shiny::showNotification("Goal time uninterpretable - defaulting to 30m")
            .goal <- lubridate::hm("00:30") # Default Value
          }
          
          # Translate goals to amounts each day
          if (!is.null(input[[paste0("goal_tf", .x)]])) {
            if (is.character(input[[paste0("goal_tf", .x)]])) {
              message("Goal_tf is character")
              .int_goal <- data.frame(tf = tfs[[input[[paste0("goal_tf", .x)]]]], stringsAsFactors = F) %>% inset('time', value = .goal) # Create Data frame with all days in the week, weekdays, or weekends
            } else {
              message("Goal_tf is numeric")
              .int_goal <- data.frame(tf = input[[paste0("goal_tf", .x)]], time = .goal, stringsAsFactors = F) 
            }
          }
          user_profile[[paste0("goal", .x)]] <<- input[[paste0("goal", .x)]] # Save the goal as a period
          user_profile[[paste0("goal_tf", .x)]] <<- ifelse(is.null(input[[paste0("goal_tf", .x)]]), "wk", input[[paste0("goal_tf", .x)]]) # Sae the timeframe as a character
          .int_goal
        }) %>% do.call("rbind.data.frame", .)
        
      } else {
        message("Default values for goal")
        # Default Values for Goal
        user_profile$goal1 <<- "30m"
        user_profile$goal_tf1 <<- "wk" 
        .int_goal <- data.frame(tf = tfs[["wk"]], stringsAsFactors = F) %>% inset("time", value = lubridate::hm("00:30"))
      }
      user_profile[["int_goal"]] <<- .int_goal # Save the hidden dataframe for backend processing
      
   
      
      # For Debug
      # input <- list(int_tf1 = "wd", int_tf2 = "we", int_begin1 = "08:00", int_begin2 = "08:00", int_end1 = "15:00", int_end2 = "17:00", int_max1 = 3, int_max2 = 1)
      # Intervention Windows Modal
      if (!is.null(input[["int_begin1"]])) {
        .int_sms_params <- purrr::map(stringr::str_extract(names(input), "(?<=int_tf)\\d") %>% purrr::keep(!is.na(.)) %>% as.numeric %>% setNames(nm = .), ~{
          if (is.character(input[[paste0("int_tf", .x)]])) {
            message("int_tf is character")
            .int_params <- data.frame(tf = tfs[[input[[paste0("int_tf", .x)]]]], stringsAsFactors = F) %>% inset('int_begin', value = input[[paste0("int_begin", .x)]]) %>% inset('int_end', value = input[[paste0("int_end", .x)]]) %>% inset('int_max', value = input[[paste0("int_max", .x)]])
          } else {
            message("int_tf is numeric")
            .int_params <- data.frame(tf = input[[paste0("int_tf", .x)]], int_begin = input[[paste0("int_begin", .x)]], int_end = input[[paste0("int_end", .x)]], int_max = input[[paste0("int_max", .x)]], stringsAsFactors = F)
          }
          shinyFeedback::feedbackWarning(session$ns(paste0("int_end", .x)), condition = {!.int_params$int_begin %>% lubridate::hm() %>% lubridate::is.period() | !.int_params$int_end %>% lubridate::hm() %>% lubridate::is.period()}, text = "Must provide a time in 24 hr format: HH:MM")
          validate(
            need(.int_params$int_begin %>% lubridate::hm() %>% lubridate::is.period() & .int_params$int_end %>% lubridate::hm() %>% lubridate::is.period(), "Please ensure all window beginnings and endings are valid times in 24 hr format: HH:MM")
          )
          user_profile[[paste0("int_begin", .x)]] <<- input[[paste0("int_begin", .x)]] # Save the interval begin
          user_profile[[paste0("int_end", .x)]] <<- input[[paste0("int_end", .x)]] # Save the interval begin
          user_profile[[paste0("int_max", .x)]] <<- input[[paste0("int_max", .x)]] # Save the interval begin
          user_profile[[paste0("int_tf", .x)]] <<- ifelse(is.null(input[[paste0("int_tf", .x)]]), "wk", input[[paste0("int_tf", .x)]]) # Sae the timeframe as a character
          return(.int_params)
        }) %>% do.call("rbind.data.frame", .)
        user_profile$int_sms <<- .int_sms_params
      }
      user_profile[["int_begin1"]] <<- input[["int_begin1"]] # Save the interval begin
      user_profile[["int_end1"]] <<- input[["int_end1"]] # Save the interval end
      user_profile[["int_max1"]] <<- input[["int_max1"]] # Save the interval max
      user_profile[["int_tf1"]] <<- ifelse(is.null(input[["int_tf1"]]), "wk", input[["int_tf1"]])
      # Save enSMS state regardless of having it filled out
      user_profile$enSMS <<- ifelse(is.null(input$enSMS), T, input$enSMS) 
      if (!is.null(input[["int_email_tf1"]])) {  
        .int_email_params <- purrr::map(stringr::str_extract(names(input), "(?<=int_email_tf)\\d") %>% purrr::keep(!is.na(.)) %>% as.numeric %>% setNames(nm = .), ~{
          if (is.character(input[[paste0("int_email_tf", .x)]])) {
            # Create Data frame with all days in the week, weekdays, or weekends
            .int_email_params <- data.frame(timepoints = purrr::map(tfs[[input[[paste0("int_email_tf", .x)]]]], .i = .x,  function(.x, .i){
              lubridate::ymd_hm(paste0(lubridate::ceiling_date(lubridate::today(), unit = "weeks", week_start = .x), " ", input[[paste0("int_email_time", .i)]]), tz = "US/Eastern")
            }) %>% do.call(c, .), stringsAsFactors = F)
          } else {
          }
          user_profile[[paste0("int_email_tf", .x)]] <<- ifelse(is.null(input[[paste0("int_email_tf", .x)]]), "wk", input[[paste0("int_email_tf", .x)]])
          user_profile[[paste0("int_email_time", .x)]] <<- input[[paste0("int_email_time", .x)]]
          return(.int_email_params)
        }) %>% do.call("rbind.data.frame", .)
        user_profile$int_email <<- .int_email_params
      } else {
        # Set Defaults
        .int_email_params <- data.frame(times = purrr::map(tfs[["wk"]] %>% setNames(nm = .), ~{
          lubridate::ymd_hm(paste0(lubridate::ceiling_date(lubridate::today(), unit = "weeks", week_start = .x), " 08:00"), tz = "US/Eastern")
        }) %>% do.call(c, .), stringsAsFactors = F)
        user_profile$int_email <<- .int_email_params
        user_profile[[paste0("int_email_tf1")]] <<- "wk"
        user_profile[[paste0("int_email_time1")]] <<- "08:00" 
      }
      user_profile$enEmail <<- ifelse(is.null(input$enEmail), T, input$enEmail)
   
   shiny::showNotification(ui = "Profile Saved!", duration = 5, type = "message")
   
  })
  output$warn <- renderText({warnText()})
  ebs <- reactiveVal(0)
  observeEvent(input$addBuddy, {
    ebs(ebs() + 1)
    
    message("Extra Buddies", ebs())
    insertUI(selector = paste0("#",session$ns("buds")),
             where = "beforeEnd",
             ui = {
               
               .newInput <- shiny::fluidRow(id = paste0("b",ebs()),
                                            column(4, textInput(session$ns(paste0("b", ebs(), "_n")), "Buddy's Name", value = input[[paste0("b", ebs(), "_n")]])), 
                                            column(4, textInput(session$ns(paste0("b", ebs(), "_pn")), "Buddy's Phone Number", value = input[[paste0("b", ebs(), "_pn")]], placeholder = "999-999-9999")), 
                                            column(4, textInput(session$ns(paste0("b", ebs(), "_em")), "Buddy's Email", value = input[[paste0("b", ebs(), "_em")]], placeholder = "example@gmail.com"))
               )
             },
             multiple = T,
             session = session
    )
  })
  output$ebUI <- renderUI({
    ebUI(session$ns, user_profile)
  })
  observeEvent(input$inviteBuddy, {
    purrr::walk(1:ebs(), ~ {
      user_profile[[paste0("b",.x,"_n")]] <<- input[[paste0("b",.x,"_n")]] # Save Name
      # ----------------------- Fri Nov 22 11:19:43 2019 ------------------------#
      # Validate buddy phone number
      if (!is.null(input[[paste0("b",.x,"_pn")]])) {
        .pn <-  stringr::str_replace_all(input[[paste0("b",.x,"_pn")]], "[^[0-9]+]+", "")
        shinyFeedback::feedbackWarning(session$ns(paste0("b",.x,"_pn")), condition = {nchar(.pn) < 10}, text = paste0("Please enter a ten digit phone number for Buddy ", .x)) # Create warning if invalid phone number
        validate(
          need(nchar(.pn) >= 10, message = paste0("Please enter a ten digit phone number for Buddy ", .x))
        )
        if (nchar(.pn) >= 10) { # if the phone number is a valid 10 digits
          # Carrier Lookup
          # UNCOMMENT IN LIVE
          .url <- httr::parse_url("https://lookups.twilio.com/v1/PhoneNumbers/")
          .url <- httr::modify_url(.url, path = paste0(.url$path,.pn))
          .url <- httr::modify_url(.url, query = list(Type = "carrier"))
          .resp <- httr::GET(.url,  httr::authenticate(twilio:::get_sid(), twilio:::get_token()))
          .carrier <- httr::content(.resp)$carrier$name
          # .carrier <- "Verizon Wireless"
          .mms <- stringr::str_replace(carrier_suffix[agrep(.carrier, carrier_suffix$Carrier), "Gateway", drop = T], "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")", .pn) # Get the mms gateway from carrier_suffix
          user_profile[[paste0("b",.x,"_pn")]] <<- c(pn = .pn, mms = .mms) # Store the 10 digit pn and the email to mms gateway
        }
      }
      # ----------------------- Fri Nov 22 11:21:23 2019 ------------------------#
      # Validate buddy email
      if (!is.null(input[[paste0("b",.x,"_pn")]])) {
        shinyFeedback::feedbackWarning(paste0("b",.x,"_em"), condition = {!stringr::str_detect(input[[paste0("b",.x,"_em")]], valid_email)}, text = paste0("Please enter a valid email for Buddy ",.x))
        validate(
          need(stringr::str_detect(input[[paste0("b",.x,"_em")]], valid_email), paste0("Please enter a valid email for Buddy ",.x))
        ) 
        user_profile[[paste0("b",.x,"_em")]] <<- input[[paste0("b",.x,"_em")]] # Save buddy email
      }
    })
    # ----------------------- Sun Nov 24 04:32:49 2019 ------------------------#
    # Map over saved buddy info in user_profile to create to field
    
    .To <- purrr::map_chr(stringr::str_extract(names(user_profile), "(?<=b)\\d") %>% purrr::keep(!is.na(.)) %>% as.numeric %>% unique, .user_profile = user_profile, function(.x, .user_profile){
      paste0("'",.user_profile[[paste0("b",.x,"_n")]],"'","<",.user_profile[[paste0("b",.x,"_em")]],">")
    }) %>% paste0(collapse = ",")
    .From <- paste0("'",user_profile$fn," ",user_profile$ln," via Be-Efficient'<sholsen@alumni.emory.edu>")
    .message <- input$b_m
    message(glue::glue("To:", .To))
    message(glue::glue("From: {.From}"))
    message(glue::glue("Message: {.message}"))
    
    # Create a Mime message and send it
    gmailr::gm_auth_configure(app = gargle::oauth_app_from_json("../sholsen@alumni.emory.eduOAuth.json", appname = "Be-Efficient_GmailR"))
    gmailr::gm_auth()
    .email_confirm <- gmailr::gm_mime(To = .To,
                                      From = .From) %>%
      gmailr::gm_subject(glue::glue("Your Friend {user_profile$fn} {user_profile$ln} invites your support in using Be-Efficient to achieve their goals!")) %>% 
      gmailr::gm_text_body(.message) %>%
      gmailr::gm_send_message()
    if (.email_confirm$labelIds[[1]] == "SENT") shinyWidgets::sendSweetAlert(session, title = "Invitations Sent!", text = "", type = "success") else warning("Email Failed to Send - Authentication Issues?")
  })
  # 'Add Additional Goals' ------
  gbs <- reactiveVal(0)
  observeEvent(input$addGoal, {
    gbs(gbs() + 1)
    message("Extra Goals", gbs())
    insertUI(selector = paste0("#",session$ns("goalrows")),
             where = "beforeEnd",
             ui = {
               .newInput <- shiny::fluidRow(id = paste0("g",gbs()),
                                            column(4, textInput(session$ns(paste0("goal", gbs())), "Total time", value = NULL, placeholder = "HH:MM, ie 1hr & 15min = 1:15")),
                                            column(4, selectInput(session$ns(paste0("goal_tf", gbs())), label = "Time Frame", choices = timeframe_choices, selected = "wk", selectize = F, width = "115px"))
               )
             },
             multiple = T
    )
  })
  output$goalsUI <- renderUI({
    goalsUI(session$ns, user_profile, timeframe_choices)
  })
  
  # 'Add Additional SMS Windows' ----
  ints <- reactiveVal(0)
  observeEvent(input$addSMS, {
    ints(ints() + 1)
    message("Extra SMS Windows: ", ints())
    insertUI(selector = paste0("#", session$ns("int_sms")),
             where = "beforeEnd",
             ui = {
               .newInput <- shiny::fluidRow(id = paste0("int", ints()),
                                            column(12, style = "display:inline-block;",
                                                   tags$table(class = "inputs", tags$tr(tags$td(
                                                     textInput(session$ns(paste0("int_begin", ints())), "Start Time", value = NULL, placeholder = "HH:MM", width = "75px")),
                                                     tags$td(tags$p("-")),
                                                     tags$td(textInput(session$ns(paste0("int_end", ints())), "End Time", value = NULL, placeholder = "HH:MM", width = "75px"))), 
                                                     tags$tr(tags$td(colspan = 2, selectInput(session$ns(paste0("int_tf", ints())), label = "Time Frame", choices = timeframe_choices, selected = "wk", selectize = F, width = "115px")), 
                                                             tags$td(numericInput(session$ns(paste0("int_max", ints())), label = "Max #/Day", value = 3, width = "75px")))))
               )
             },
             multiple = T
    )
  })
  output$smsUI <- renderUI({
    smsUI(session$ns, user_profile, timeframe_choices)
  })
  # 'Add Email Windows' ----
  ems <- reactiveVal(0)
  observeEvent(input$addEmail, {
    ems(ems() + 1)
    message("Extra Email Windows: ", ems())
    insertUI(selector = paste0("#",session$ns("int_email")),
             where = "beforeEnd",
             ui = {
               .newInput <- shiny::fluidRow(class = paste0("int_email", ems()),
                                            column(6, style = "display:inline-block;", 
                                                   tags$table(class = "inputs", tags$tr(
                                                     tags$td(textInput(session$ns(paste0("int_email_time", ems())), "Send time", value = "08:00", placeholder = "HH:MM", width = "100px")),
                                                     tags$td(selectInput(session$ns(paste0("int_email_tf", ems())), label = "Time Frame", choices = timeframe_choices, selected = "wk", selectize = F, width = "115px")))))
               )
             },
             multiple = T
    )
  })
  output$emailUI <- renderUI({
    emailUI(session$ns, user_profile, timeframe_choices)
  })
  observeEvent(user_profile, {assign("user_profile", user_profile, .GlobalEnv)})
}