library(shiny)
library(htmltools)
library(plotly)
library(lubridate)
shinyUI(fluidPage(theme = shinythemes::shinytheme("cerulean"),
    tags$head(
      
        tags$style(HTML("
      .input-color {
    position: relative;
    display: inline-block;
    float:left;
    width: 200px;
    padding-top: 35px;
}
.input-color .leg {
  padding-right: 10px;
  padding-left: 10px;
}
.input-color .color-box {
    width: 20px;
    height: 20px;
    text-align: center;
    vertical-align: middle;
    padding:5px;
    display: inline-block;
}
.input-color .label {
    padding-left:10px;
    display: inline-block;
    border-right:1px; black;
    color: black;
}
.white {
    color: white;
}
    "))
    ),
        # shinythemes::themeSelector(),
        # Application title
        # h1("Be", span("Efficient", style = "font-weight: 300"),
        #    style = "font-family: 'Source Sans Pro';
        #     color: #fff; text-align: left;
        #     background-image: url('texturebg.png');
        #     padding: 20px"),
        # br(),
    
        
        
        mainPanel(width = 12,
            fluidRow(style = "width:100%", # Bootstrap Row
                    column(6,  # Bootstrap 3/4 width column
                        strong("Pick the Date range for Rescuetime data over which we will derive your predicted intervention windows."), # Instructions on date range slider
                        strong("The ideal time period is one over which you feel your routines have been relatively stable.")),
                    column(6, style = "display:inline-block",
                      tags$table(style = "vertical-align:middle;",
                        tags$tbody(
                          tags$tr(
                        tags$td(sliderInput( ".date_range", "Date Range:", # Date Range slider
                          min = {lubridate::today() - lubridate::weeks(24)}, # Allows up to 6 months of data
                          max = lubridate::today(),
                          value = c({lubridate::today() - lubridate::weeks(12)}, lubridate::today()),
                          timezone = Sys.timezone(location = TRUE),
                          ticks = T,
                          timeFormat = "%F")),
                        tags$td(actionButton(style = "display:inline-block; margin:20px", inputId = "get_data", label = "Draw", icon = icon("chart-bar", lib = "font-awesome")))
                         )
                        )
                      )
                    )
                    ),
            fluidRow(column(10, plotlyOutput("int_plot")),
                    column(2,
                        HTML("<div class='input-color'>
        <table>
        <tr>
        <td class='leg'>
        <div class='color-box white' style='background-color: #0055c4;'>2</div><p class='label'>Very Productive</span>
        </td>
        </tr>
        <tr>
        <td class='leg'>
        <div class='color-box white' style='background-color: #3d80e0;'>1</div><p class='label'>Productive</span>
        </td>
        </tr>
        <tr>
        <td class='leg'>
        <div class='color-box' style='background-color: #dc685a;'>-1</div><p class='label'>Distracting</span>
        </td>
        </tr>
        <tr>
        <td class='leg'>
        <div class='color-box' style='background-color: #d61800;'>-2</div><p class='label'>Very Distracting</span>
        </td>
        </tr>
        <tr>
        <td class='leg'>
        <div class='color-box' style='background-color: #d61800;outline-color: #00ff00;outline:3px solid'></div><p class='label'>Intervention Window</span>
        </td>
        </tr>
        </table>
        </div>"))
                  )
        
      )
  )
)