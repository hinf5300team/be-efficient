---
title: "Title"
author: "Stephen Synchronicity"
date: '`r format(Sys.time(), "%Y-%m-%d")`'
always_allow_html: yes
header-includes:
   - \usepackage{dcolumn}
output: 
  html_document: 
    self_contained: yes
    css: C:\Users\Administrator\Documents\R\win-library\3.5\neuhwk\rmarkdown\templates\DA5030\resources\bootstrap.min.css
    highlight: zenburn
    keep_md: no
    theme: spacelab
    toc: TRUE
    toc_float: TRUE
    df_print: paged
    code_folding: hide
---
```{r setup, include=FALSE}
# Knitr Options
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE, cache = TRUE, fig.align = 'center', fig.height = 5, fig.width = 7.5, tidy = TRUE, tidy.opts = list(width.cutoff = 80))
options(scipen = 12)
# Make reproducible
set.seed(1)
# Load packages
HDA::startPkgs(c("tidyverse","htmltools","magrittr","printr"))
# # Attach dependencies
# rmarkdown::html_dependency_jquery()
# rmarkdown::html_dependency_bootstrap("spacelab")
# rmarkdown::html_dependency_jqueryui()
# # Use Chunk Titles to add Bold Headings to Chunks
# source("~/R/Scripts/addChunkTitles.R")
# rmd <- addChunkTitles(rstudioapi::getSourceEditorContext()$path)
# write(rmd,file=(rstudioapi::getSourceEditorContext()$path))
# # HTML to Latex
# source("~/R/Scripts/HTMLtoLatex.R")
# rmd <- HTMLtoLatex(rstudioapi::getSourceEditorContext()$path)
# write(rmd,file=(rstudioapi::getSourceEditorContext()$path))
```

# Rescuetime 
```{r 'Initial plot',fig.dim=(7.5, 7.5), eval = F}
# ----------------------- Wed Oct 16 06:25:33 2019 ------------------------#
# Distracting time visualization based on daily rescuetime data for presentation
googlesheets::gs_auth(token = "~//R//husky_googlesheets_token.rds")
gs <- googlesheets::gs_url("https://docs.google.com/spreadsheets/d/1eLzf3ODfkXJvQutzhUtNo9GBGh97Yl9w_XQlqkH5hGw/edit?usp=drive_web&ouid=109343641324722124628")
rt_dat <- googlesheets::gs_read(gs)


# library(tidyverse)
# library(magrittr)
names(rt_dat) %<>% str_replace("\\:","") 
rt_dat %<>%
  select(`date`, `all_productive_duration_formatted`, all_productive_hours, all_distracting_duration_formatted,all_distracting_hours) %>%
  mutate(week_day = lubridate::wday(`date`, label = T)) %>% 
  mutate(week_of_year = lubridate::isoweek(`date`)) %>%
  mutate_at(vars(starts_with("week")), ~ as.factor(.)) %>%
  mutate(total_distracting = ifelse(all_distracting_hours - all_productive_hours > 0, all_distracting_hours - all_productive_hours, 0)) 
.rt_gg_ex <- ggplot(data = rt_dat, aes(x = date, y = total_distracting))+
  geom_point(aes(color = week_day))+
  geom_line(color = "indianred")+
  theme(legend.position = "right")
g_legend<-function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)}
.legend <- g_legend(.rt_gg_ex)
rt_gg <- rt_dat %>%
  filter(date >= lubridate::ymd("2019-09-16") & date <= lubridate::ymd("2019-09-29")) %>% 
  split(f = .[["week_of_year"]]) %>%
  purrr::keep(.p = ~ nrow(.) == 7) %>% 
  purrr::map(.f = function(.x){
    .rect_aes <- list(
      .x_min = .x[5, "date"],
      .x_max = .x[7, "date"],
      .y_min = min(.x$total_distracting),
      .y_max = max(.x$total_distracting)
    )
    ggplot(data = .x, aes(x = date, y = total_distracting))+
  geom_point(aes(color = week_day))+
  geom_line(color = "indianred")+
  geom_rect(aes(xmin = .x[5, "date", drop = T],
      xmax = .x[7, "date", drop = T],
      ymin = min(.x$total_distracting),
      ymax = max(.x$total_distracting)), fill = "lightblue", alpha = 1/10)
  })
library(ggpubr)
ggarrange(plotlist = rt_gg, common.legend = T, nrow = 2, font.label = list(size = 12))
ggsave("Weeks.png", device = "png", width = 7.5, height = 6, units = "in")
  
```

```{r 'Granular graph for PP'}
# ----------------------- Sun Oct 20 21:11:53 2019 ------------------------#
# Get data from the API
source("Rescuetime-rt_getData.R")
rt_dat <- rt_getData(key = keyring::key_get("Rescuetime", "key"), .rk = "productivity", .i = "min", .begin = lubridate::today() - lubridate::weeks(5))
rt_dat %>% View

```
```{r 'Xform Data'}
rt_daily <- rt_dat %>%
  select(- `Number of People`) %>% 
  rename(`Time(min)` = `Time Spent (seconds)`) %>% 
  filter(Productivity != 0) %>% 
  mutate_at(vars(`Time(min)`), ~ {. / 60}) %>% # Minutes
  mutate_at(vars(`Time(min)`), list(~ sign(Productivity) * .)) %>% # Distracting to negative
  mutate_at(vars(Productivity), ~ factor(., levels = c(2, 1, -1, -2))) %>% 
  mutate(Bins = lubridate::floor_date(Date, unit = "15 minutes")) %>% # Add 15 min bins
  group_by(Bins, Productivity) %>% 
  summarize_at(vars(`Time(min)`), sum) %>%
  ungroup %>% 
  mutate(Wday = lubridate::wday(Bins, week_start = 1, label = T)) %>%  # Add the Weekday
  assign("rt_pred", . , envir = .GlobalEnv) %>% # create intermedate variable for prediction
  mutate_at(vars(Bins), ~ {strftime(as.character(.), format = "%H:%M") %>% str_replace("\\:","") %>% as.numeric}) %>% # Convert to H:M format
  group_by(Bins, Wday, Productivity) %>%
  summarize_at(vars(`Time(min)`), mean) %>%  # Summarize by the 15m bins
  mutate(PD = factor(ifelse(as.numeric(as.character(Productivity)) > 0, "P", "D"), levels = c(P = "P", D = "D"))) %>% # Add Binary Productive/Distracting Factor 
  ungroup %>% 
  mutate_at(vars(Bins), ~ lubridate::ymd_hm(paste0(lubridate::today()," ",.), tz = "EST"))
rt_hrly <- rt_daily %>% 
  mutate(Hr = lubridate::hour(Bins)) %>% 
  mutate_at(vars(Hr), ~ lubridate::ymd_hm(paste0(lubridate::today()," ",.,":00"), tz = "EST")) %>% # Group by Hours
  group_by(Hr, Wday, Productivity, PD) %>% 
  summarize_at(vars(`Time(min)`), sum)


```

```{r 'Predict Distraction by Ranking method'}


rt_window_pred <- left_join(
# Tally method of predicting distraction
rt_pred %>% 
  mutate(PD = factor(ifelse(as.numeric(as.character(Productivity)) > 0, "P", "D"), levels = c(P = "P", D = "D"))) %>% # Add Binary Productive/Distracting Factor 
  group_by(Wday, Bins, PD) %>% 
  summarize_at(vars(`Time(min)`), sum) %>% # sum by PD
  ungroup %>% 
  mutate(Int10 = ifelse(PD == "D" & `Time(min)` < -5, T, F)) %>% # Add Binary for whether Distracting time exceeded 5 min in a 15m window to use as a tally for Intervention windows
  mutate_at(vars(Bins), ~ strftime(as.character(.), "%H:%M", tz = "America/New_York")) %>% # Convert to H:M format
  group_by(Wday, Bins) %>%
  summarize_at(vars(Int10), sum), 
# Average method of predicting distraction
rt_pred %>% 
  mutate(PD = factor(ifelse(as.numeric(as.character(Productivity)) > 0, "P", "D"), levels = c(P = "P", D = "D"))) %>% # Add Binary Productive/Distracting Factor 
  group_by(Wday, Bins, PD) %>% 
  summarize_at(vars(`Time(min)`), sum) %>% # sum by PD
  ungroup %>%
  mutate_at(vars(Bins), ~ strftime(., "%H:%M", tz = "America/New_York")) %>% # Convert to H:M format
  group_by(Wday, Bins, PD) %>%  
  summarize_at(vars(`Time(min)`), mean) %T>% # mean by PD
  {assign("q5", qnorm(.05, mean(filter(., PD == "D") %>% .[["Time(min)"]]), sd(filter(., PD == "D") %>% .[["Time(min)"]])), .GlobalEnv)} %>% 
  ungroup %>% 
  mutate(Int.5 = `Time(min)` < q5)) %>% 
  mutate(Int_Window = {Int10 > 0 & Int.5 == T}) %>% 
  mutate_at(vars(Bins), ~ lubridate::ymd_hm(paste0(lubridate::today()," ",.), tz = "America/New_York"))

```

```{r 'Generate Daily Overview Plot'}
.rt_col <- c("2" = "#0055c4", "1" = "#3d80e0", "-1" = "#dc685a", "-2" = "#d61800")
# Function to shift bars to right of tick marks
# TODO does not function properly for datetimes
shiftBar <- function(.) {
  .x <- sort(unique(.))
  if (lubridate::is.POSIXct(.)) .out <- round(difftime(.x[2],.x[1]) / 2, 0) else .out <- {.x[2] - .x[1]} / 2
  return(. + .out)
}
int_plot <- left_join(rt_daily, rt_window_pred %>% select(Wday, Bins, PD, Int_Window), by = c("Wday","Bins", "PD")) %>% 
  filter(Wday == lubridate::wday(lubridate::today(), label = T)) %>% 
  ggplot(data = ., aes(x = shiftBar(Bins), # this shifts the bar to the right of the tick which makes more sense for time
                       y = `Time(min)`,
                       fill = Productivity,
                       text = paste0("Minutes: ", round(`Time(min)`)))) +
    geom_col(data = rt_hrly %>% filter(Wday == lubridate::wday(lubridate::today(), label = T)), aes(x = Hr + lubridate::minutes(30)), # this shifts the bar to the right of the tick which makes more sense for time
             alpha = .5) + 
    geom_col(position = "stack", aes(color = Int_Window)) +
    geom_vline(xintercept = lubridate::now())+
    scale_x_datetime(date_labels = "%H", date_breaks = "1 hour") +
    scale_fill_manual(values = .rt_col) +
    scale_y_continuous(breaks = function(lims){
      .upper <- 15 * seq_along(1: {lims[2] / 15})
      .lower <- -15 * seq_along(1: {abs(lims[1]) / 15})
        return(unlist(c(.lower,.upper)))}) +
    scale_color_manual(name = "Intervention\nWindows", limits = c("TRUE"), values = c("TRUE" = "green")) +
    labs(title = "Daily Overview",
    subtitle = "Today's Intervention Opportunities",
    caption = "",
    x = "Hour of day",y = "Minutes") +
    theme(plot.title = element_text(hjust = .5),plot.subtitle = element_text(hjust = .5), legend.position = "none")
plotly::ggplotly(int_plot, tooltip = c("text"))
```

```{r 'Save the Daily Overview Graph', eval = F}
ggplot2::ggsave("Daily_Overview.png", plot = int_plot, device = "png", width  = 5, height = 2, units = "in")
```


```{r 'Xform RT Data'}
rt_dat %>% 
            select(- `Number of People`) %>% 
             rename(`Time(min)` = `Time Spent (seconds)`) %>% 
             filter(Productivity != 0) %>% 
  mutate_at(vars(`Time(min)`), ~ {. / 60}) %>% # Minutes
  mutate_at(vars(`Time(min)`), list(~ sign(Productivity) * .)) %>% # Distracting to negative
  mutate_at(vars(Productivity), ~ factor(., levels = c(2, 1, -1, -2))) %>% 
  mutate(Bins = lubridate::floor_date(Date, unit = "1 hour")) %>% # Add 15 min bins
  group_by(Bins, Productivity) %>% 
  summarize_at(vars(`Time(min)`), sum) %>%
  ungroup %>% 
  mutate(wday = lubridate::wday(Bins, week_start = 1, label = T),
         wnum = lubridate::wnum(Bins))
            ungroup %>%
            group_by(Bins, wday, wnum, Productivity) %>%
            summarize_at(vars(`Time(min)`), sum) %>% # Summarize time by the new_productivity factor
            

rt_prod <- rt_dat %>% 
  group_by(Date, Productivity) %>% 
  filter(Productivity != 0) %>%  # filter netural
  summarize(total = sum(lubridate::seconds(`Time Spent (seconds)`)), New_Prod = (Vectorize(function(.){ifelse(. < 0, "D", "P")})(Productivity))) %>% # create total with time grouped by Date, Convert all productive/distracting levels to a single factor D/P respectively
  ungroup %>%
  group_by(Date, New_Prod) %>%
  summarize(total = sum(total)) %>% # Summarize time by the new_productivity factor
  spread(key = New_Prod, value = total) %>% # Spread those totals to columns for subtraction
  mutate_all(~ ifelse(is.na(.), 0, .)) %>%  # all NA to 0
  mutate(wnum = lubridate::isoweek(Date), wday = lubridate::wday(Date, week_start = 1, label = T), hr = strftime(as.character(Date), "%H:%M", tz = "America/New_York")) %>%  # Create a productivity factor D/P again # add weekday and weeknum labels and final metric productive - distraction
  mutate_at(vars(Prod), ~ factor(., levels = c(P = "P", D = "D"))) %>%  # Switch the order for the ggplot legend
  mutate_at(vars(metric, D, P), ~ {. / 60}) # convert seconds to minutes
rt_dat %>% 
            group_by(Date, Productivity) %>% 
            filter(Productivity != 0) %>%  # filter netural
            summarize(total = sum(lubridate::seconds(`Time Spent (seconds)`)), New_Prod = (Vectorize(function(.){ifelse(. < 0, "D", "P")})(Productivity))) %>% # create total with time grouped by Date, Convert all productive/distracting levels to a single factor D/P respectively
            ungroup %>%
            group_by(Date, New_Prod) %>%
            summarize(total = sum(total)) %>% # Summarize time by the new_productivity factor
            spread(key = New_Prod, value = total) %>% # Spread those totals to columns for subtraction
            mutate_all(~ ifelse(is.na(.), 0, .)) %>%  # all NA to 0
            mutate(wnum = lubridate::isoweek(Date), wday = lubridate::wday(Date, week_start = 1, label = T), hr = lubridate::hour(Date))
```

```{r 'Graph of today: hourly'}
.int_window_pred %>% 
  filter(wday == lubridate::wday(lubridate::today(), label = T)) %>%
  mutate_at(vars(hr), ~ lubridate::ymd_hm(paste0(lubridate::today()," ",.,":00"))) %>% 
  mutate(Prod = {ifelse(mu_metric > 0, "P", "D")}) %>% 
  mutate_at(vars(Prod), ~ factor(., levels = c("P","D"))) %>% 
  mutate(temp_rank = dense_rank(Agg_rank)) %>% 
  mutate(int_windows = ifelse(temp_rank <= 3, "Window", "None")) %>% 
  ggplot(aes(x = hr, y = mu_metric, fill = Prod, color = int_windows))+ 
  geom_bar(stat = "identity")+
  # geom_rect(aes(xmin = ,
  #     xmax = .x[7, "Date", drop = T],
  #     ymin = min(.x$total_distracting),
  #     ymax = max(.x$total_distracting)), fill = "lightblue", alpha = 1/10)+
  scale_x_datetime(labels = scales::time_format(format = "%H"), date_breaks = "1 hour")+
  scale_fill_manual(name = "Productivity", values = c("P" = "royalblue", "D" = "indianred"), drop = F)+
  scale_color_manual(name = "Intervention\nWindows", limits = c("Window"), values = c(Window = "red"))+
  labs(title = "Daily Overview",
  subtitle = "Today's Intervention Opportunities",
  caption = "",
  x = "Hour of day",y = "Minutes") +
  theme(plot.title = element_text(hjust = .5),plot.subtitle = element_text(hjust = .5))
ggplot2::ggsave("Daily_Overview.png", device = "png", width  = 5, height = 2, units = "in")

```

```{r 'Graph RT data', fig.dim = c(10.5, 8)}
library(ggpubr)
(rt_gg <- rt_prod %>% 
  filter(Date >= lubridate::today() - lubridate::weeks(3)) %>% # filter all but the past two weeks
  split(f = .[["wnum"]]) %>% # split by the weeknum
  purrr::keep(.p = ~ length(unique(.[["wday"]])) == 7) %>% #Filter for whole weeks only
  purrr::map2(.y = seq_along(.), .l = length(.), .tD = rt_topD,  .f = function(.x, .y, .l, .tD){ # Graph each
    if (.y == 1) .subtitle <- "Activities by Hour" else .subtitle <- NULL # If it's the first one then add a subtitle (so as not to clutter pubr graph with text)
    if (.y == .l) .caption <- "Digital Activity Blue/Red computed as Productive - Distracting" else .caption <- NULL # If its the last graph add the caption
    
    library(scales)
  .g <- ggplot(data = .x, aes(x = Date, y = metric)) +
    geom_bar(stat = "identity", aes(fill = Prod)) +
    # geom_hline(yintercept = 60)+
    # geom_text(data = .x %>% group_by(wday) %>% top_n(n = -1, wt = Date), aes(y = 60, x = Date), nudge_x = 3600, label = "1 Hr")+
    # geom_hline(yintercept = -60)+
    # geom_text(data = .x %>% group_by(wday) %>% top_n(n = -1, wt = Date), aes(y = -60, x = Date), nudge_x = 3600, label = "1 Hr")+
    scale_x_datetime(date_minor_breaks = "hours", date_labels = "%H")+
    scale_fill_manual(values = c(P = "royalblue", D = "indianred"), name = "Productivity")+
    scale_y_continuous(breaks = function(lims){
      .upper <- 15 * seq_along(1: {lims[2] / 15})
      .lower <- -15 * seq_along(1: {abs(lims[1]) / 15})
      return(unlist(c(.lower,.upper)))})+
    facet_grid(cols = vars(wday), scales = "free_x", space = "free_x")+
    theme(plot.title = element_text(hjust = .5, size = 20),
          plot.subtitle = element_text(hjust = .5, size = 18),
          plot.caption =  element_text(size = 16),
          axis.text.x = element_text(angle = 45),
          axis.title.y = element_text(size = 18))+
    labs(title = paste0("Week of ", lubridate::as_date(min(.x$Date))),
    subtitle = .subtitle,
    caption = .caption,
    x = "Time",y = "Total (min)")
  
  # geom_rect(aes(xmin = .x[5, "Date", drop = T],
  #     xmax = .x[7, "Date", drop = T],
  #     ymin = min(.x$total_distracting),
  #     ymax = max(.x$total_distracting)), fill = "lightblue", alpha = 1/10)
  }))

  
ggarrange(plotlist = rt_gg, common.legend = T, nrow = 2)
ggplot2::ggsave("Weeks.png", device = "png", width = 1366 * .75 / 72, height = 788 * .8 / 72, units = "in", dpi = 72)
```


```{r 'XgBoost'}
#Xgboost
library(xgboost)
xg <- xgboost::xgboost(data = rt_prod[,c("wday","hr")] %>% as.matrix, nrounds = 250, label = rt_prod$metric)
predict(xg, newdata = rt_prod[,c("wday","hr")] %>% as.matrix)
# RMSE is way too high
```

```{r 'KNN'}
library(caret)
.t_ind <- caret::createDataPartition(rt_prod$metric, list = F, p = .75)
.train <- rt_prod[.t_ind, ]
.test <- rt_prod[-.t_ind, ]
rt_knn <- caret::knnreg(formula = metric ~ wday + hr + Date, data = .train, k = 5)
rt_pred <- predict(rt_knn, newdata = .test)
#RMSE still too high
```

# Google Fit
```{r 'Get Google Fit Activity Type values'}
activity_ref <- tibble::tribble(
                             ~Activity.Type, ~Integer.Value,
                                 "Aerobics",              9,
                                  "Archery",            119,
                                "Badminton",             10,
                                 "Baseball",             11,
                               "Basketball",             12,
                                 "Biathlon",             13,
                                  "Biking*",              1,
                               "Handbiking",             14,
                          "Mountain biking",             15,
                              "Road biking",             16,
                                 "Spinning",             17,
                        "Stationary biking",             18,
                           "Utility biking",             19,
                                   "Boxing",             20,
                             "Calisthenics",             21,
                         "Circuit training",             22,
                                  "Cricket",             23,
                                 "Crossfit",            113,
                                  "Curling",            106,
                                  "Dancing",             24,
                                   "Diving",            102,
                                 "Elevator",            117,
                               "Elliptical",             25,
                                "Ergometer",            103,
                                "Escalator",            118,
                                  "Fencing",             26,
                      "Football (American)",             27,
                    "Football (Australian)",             28,
                        "Football (Soccer)",             29,
                                  "Frisbee",             30,
                                "Gardening",             31,
                                     "Golf",             32,
                         "Guided Breathing",            122,
                               "Gymnastics",             33,
                                 "Handball",             34,
                                     "HIIT",            114,
                                   "Hiking",             35,
                                   "Hockey",             36,
                         "Horseback riding",             37,
                                "Housework",             38,
                              "Ice skating",            104,
                              "In vehicle*",              0,
                        "Interval Training",            115,
                             "Jumping rope",             39,
                                 "Kayaking",             40,
                      "Kettlebell training",             41,
                               "Kickboxing",             42,
                              "Kitesurfing",             43,
                             "Martial arts",             44,
                               "Meditation",             45,
                       "Mixed martial arts",             46,
                                 "On foot*",              2,
    "Other (unclassified fitness activity)",            108,
                           "P90X exercises",             47,
                              "Paragliding",             48,
                                  "Pilates",             49,
                                     "Polo",             50,
                              "Racquetball",             51,
                            "Rock climbing",             52,
                                   "Rowing",             53,
                           "Rowing machine",             54,
                                    "Rugby",             55,
                                 "Running*",              8,
                                  "Jogging",             56,
                          "Running on sand",             57,
                      "Running (treadmill)",             58,
                                  "Sailing",             59,
                             "Scuba diving",             60,
                            "Skateboarding",             61,
                                  "Skating",             62,
                            "Cross skating",             63,
                           "Indoor skating",            105,
           "Inline skating (rollerblading)",             64,
                                   "Skiing",             65,
                      "Back-country skiing",             66,
                     "Cross-country skiing",             67,
                          "Downhill skiing",             68,
                              "Kite skiing",             69,
                            "Roller skiing",             70,
                                 "Sledding",             71,
                                 "Sleeping",             72,
                              "Light sleep",            109,
                               "Deep sleep",            110,
                                "REM sleep",            111,
               "Awake (during sleep cycle)",            112,
                             "Snowboarding",             73,
                               "Snowmobile",             74,
                              "Snowshoeing",             75,
                                 "Softball",            120,
                                   "Squash",             76,
                           "Stair climbing",             77,
                   "Stair-climbing machine",             78,
                  "Stand-up paddleboarding",             79,
                      "Still (not moving)*",              3,
                        "Strength training",             80,
                                  "Surfing",             81,
                                 "Swimming",             82,
                    "Swimming (open water)",             84,
                 "Swimming (swimming pool)",             83,
                 "Table tennis (ping pong)",             85,
                              "Team sports",             86,
                                   "Tennis",             87,
  "Tilting (sudden device gravity change)*",              5,
           "Treadmill (walking or running)",             88,
     "Unknown (unable to detect activity)*",              4,
                               "Volleyball",             89,
                       "Volleyball (beach)",             90,
                      "Volleyball (indoor)",             91,
                             "Wakeboarding",             92,
                                 "Walking*",              7,
                        "Walking (fitness)",             93,
                          "Nording walking",             94,
                      "Walking (treadmill)",             95,
                       "Walking (stroller)",            116,
                                "Waterpolo",             96,
                            "Weightlifting",             97,
                               "Wheelchair",             98,
                              "Windsurfing",             99,
                                     "Yoga",            100,
                                    "Zumba",            101
  )


```

```{r 'Explore Gfit data'}
# ----------------------- Mon Oct 28 20:30:05 2019 ------------------------#
# 
.test <- purrr::map(gfit_data, .a_ref = activity_ref, function(.x, .a_ref){
  # Convert time bounds to datetime
  .x$minStartTimeNs <- purrr::pluck(.x, "minStartTimeNs") %>% RGoogleFit::NanosToPOSIXct() %>% lubridate::as_datetime(tz = "EST")
  .x$maxEndTimeNs <- purrr::pluck(.x, "maxEndTimeNs") %>% RGoogleFit::NanosToPOSIXct() %>% lubridate::as_datetime(tz = "EST")
  message(.x[["point"]] %>% class )
  # Unnest the list column 'value'
  .x[["point"]] %<>% as_tibble() %>% unnest() 
  # Change modifiedTimeMillis to datetime
  if(is_tibble(.x[["point"]]) & "modifiedTimeMillis" %in% names(.x[["point"]])) .x[["point"]] %<>% mutate_at(dplyr::vars(modifiedTimeMillis), ~ {{as.numeric(.) * 1e-3} %>% lubridate::as_datetime(tz = "EST")}) else if(length(.x[["point"]]) < 1) .x <- NULL
  # Change startTimeNanos and endTimeNanos to datetime
  if (all(c("startTimeNanos","endTimeNanos") %in% names(.x$point))) {
  .x[["point"]] %<>% mutate_at(vars(startTimeNanos, endTimeNanos), ~ {RGoogleFit::NanosToPOSIXct(.) %>% lubridate::as_datetime(tz = "EST")}) 
    }
  # Remove filler columns if present
  if ("mapVal" %in% names(.x[["point"]])){
    if (all(purrr::map(.x[["point"]]$mapVal, ~length(.)) %>% unlist == 0)) {
      .x[["point"]] %<>% select( - mapVal)
    }
  }
  # if ("intVal" %in% names(.x[["point"]])){
  # 
  #     .x[["point"]] %<>% mutate(actClass = (Vectorize(function(., .a_ref) {.a_ref[which(.a_ref$Integer.Value == .), "Activity.Type", drop = T]})(intVal, .a_ref)))
  # 
  #   
  #   .act <- purrr::map_chr(.test[[1]]$point$intVal, .a_ref = activity_ref, function(., .a_ref) {
  #     .a_ref[which(.a_ref[["Integer.Value"]] == .), "Activity.Type", drop = T]})
  #   .test[[1]]$point %>% mutate(actClass = classAct(intVal, activity_ref))
  # }
return(.x)  
}) %>% purrr::compact()
View(.test)
purrr::map(.test, ~ purrr::pluck(.x, "dataSourceId"))
.test[[1]]$point
activity_ref %>% arrange(Integer.Value)

```

