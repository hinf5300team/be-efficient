# ----------------------- Sun Oct 20 15:29:16 2019 ------------------------#
#' @title rt_getData 
#' Function to retrieve Rescuetime data given an API key
#' @param key \code{(char)} \strong{Required} Your API key obtained via the [Rescuetime Key management API](https://www.rescuetime.com/anapi/manage). \emph{Warning:} Do not hardcode this data into a script destined for a public repo! Check out the [keyring package](https://github.com/r-lib/keyring) when working in a public repo.
#' @param .by \code{(char)} Defaults to "interval". Consider this the X - axis of the returned data. It is what determines how your data is crunched serverside for ordered return.
#' \itemize{
#'  \item \code{interval} \emph{(default)} Organized around calendar time.
#'  \item \code{rank}  Organized around a calculated value, usually a sum like time spent.
#'  \item \code{member} For organization accounts, organized by member.
#' }
#' @param .i \code{(char)} \emph{Interval}  Default is "hour". In an interval report, the X axis unit. In other words, data is summarized into chunks of this size. "minute" will return data grouped into five-minute buckets, which is the most granular view available.
#' @param .begin \code{(POSIXct/Date)} Default is 3 months ago. Sets the start day for data batch, inclusive (always at time 00:00, start hour/minute not supported). Format ISO 8601 "YYYY-MM-DD"
#' @param .end  Defaults to today. Sets the end day for data batch, inclusive (always at time 00:00, end hour/minute not supported). Format ISO 8601 "YYYY-MM-DD"
#' @param .rk \code{(char)} \emph{restrict kind} Default is "overview". Allows you to preprocess data through different statistical engines. The perspective dictates the main grouping of the data, this provides different aspects of that main grouping.
#' \itemize{
#'  \item \code{overview} sums statistics for all activities into their top level category
#'  \item \code{category} sums statistics for all activities into their sub category
#'  \item \code{activity} sums statistics for individual applications / web sites / activities
#'  \item \code{productivity} productivity calculation
#'  \item \code{efficiency} efficiency calculation (\code{.by} must be "interval")
#'  \item \code{document} sums statistics for individual documents and web pages
#' } 
#' @param .format \emph{Output formats}. Defaults to CSV.
#' \itemize{
#' \item \code{CSV} - layout provides rows of comma separated data with a header for column names at top.
#' \item \code{JSON} - 
#'  \itemize{
#'   \item \code{notes} - String, a short explanation of the data envelope
#'   \item \code{row_headers} - Array, a label for the contents of each index in a row, in the order they appear in row
#'   \item \code{row_headers} - Array X Array, an array of data rows, where each row is itself an array described by the row_headers
#'  }}
#' @param .rt \code{(char)} \emph{Optional} \emph{restrict thing} The name of a specific overview, category, application or website. For websites, use the domain component only if it starts with "www", eg. "www.nytimes.com" would be "nytimes.com". The easiest way to see what name you should be using is to retrieve a list that contains the name you want, and inspect it for the exact names.
#' @param .ry \code{(char)} \emph{Optional} \emph{restrict thingy} Refers to the specific "document" or "activity" we record for the currently active application, if supported. For example, the document name active when using Microsoft Word. Available for most major applications and web sites. Let us know if yours is not.
#' @param .group \code{(char)} \emph{Optional} Provide the name of the group. Service will match that with your account to find group. Avoid re-using the same name for multiple groups when creating them. Currently we do not prevent this. First match will be returned.

# 
# rt_getData <- function(key = NULL, .by = "interval", .i = "hour", .begin = lubridate::today() - lubridate::weeks(12), .end = lubridate::today(), .rk = "overview", .format = "csv", .rt = NULL, .ry = NULL, .group = NULL) {
#   # For Debug
#     # list(key = keyring::key_get("Rescuetime", "key"), .by = "interval", .i = "minute", .begin = lubridate::ymd("2019-11-16"), .end = lubridate::now(), .rk = "productivity", .format = "CSV", .rt = NULL, .ry = NULL, .group = NULL) %>% list2env(envir = .GlobalEnv)
#   # End Debug
#   .rt_url <- httr::parse_url("https://www.rescuetime.com/anapi/data")
#   # Smart select .i
#   if (stringr::str_detect(.i, "^[Mm][Ii]")) .i <-  "minute" else 
#     if (stringr::str_detect(.i, "^[Hh]")) .i <- "hour" else
#       if (stringr::str_detect(.i, "^[Dd]")) .i <- "day" else
#         if (stringr::str_detect(.i, "^[Ww]")) .i <- "week" else
#           if (stringr::str_detect(.i, "^[Mm][Oo]")) .i <-  "month"
#   # Large by minute requests
#   if (.i == "minute" & difftime(lubridate::as_datetime(.end), lubridate::as_datetime(.begin), "days") > 28) {
#     # Create intervals of appropriate segments to suit the API restrictions
#     .seg <- ceiling(difftime(lubridate::as_datetime(.end), lubridate::as_datetime(.begin), "days") / 28)
#     .segs <- purrr::map(1:.seg, ~{
#       if (.end - lubridate::weeks(4 * .x) < .begin) .e <- .begin else .e <- .end - lubridate::weeks(4 * .x)
#       lubridate::interval(start = .e,
#                           end = .end - lubridate::weeks(4 * {.x - 1}))
#       })
#     # Sequence the requests
#     .dat <- purrr::map(.segs, ~{
#     # build query
#     .query <- list(key = key,
#                    by = .by,
#                    interval = .i,
#                    restrict_begin = lubridate::int_start(.x),
#                    restrict_end = lubridate::int_end(.x),
#                    restrict_kind = .rk,
#                    restrict_thing = .rt,
#                    restring_thingy = .ry,
#                    group = .group,
#                    format = .format
#     ) %>% purrr::compact()
#     .rt_url$query <- .query
#     .q_url <- httr::build_url(.rt_url)
#     message(paste0("Request URL: ", .q_url))
#     .resp <- httr::GET(url = .q_url)
#     if (grepl(.format, "CSV", ignore.case = T)) .dat <- readr::read_csv(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
#     if (grepl(.format, "JSON", ignore.case = T)) .dat <- jsonlite::fromJSON(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
#     return(.dat)
#     })
#     .dat <- dplyr::bind_rows(.dat) # Bind the responses
#     .dat <- .dat[!duplicated(.dat$Date), ] # Remove duplicates
#     .dat <- dplyr::arrange(.dat, Date) # arrange by date
#   } else {
#     # browser()
#     # Normal query
#     .query <- list(key = key,
#                    perspective = .by,
#                    interval = .i,
#                    restrict_begin = lubridate::as_date(lubridate::today() - lubridate::days(2)),
#                    restrict_end = lubridate::as_date(.end),
#                    restrict_kind = .rk,
#                    restrict_thing = .rt,
#                    restring_thingy = .ry,
#                    group = .group,
#                    format = .format
#     ) %>% purrr::compact()
#     .rt_url$query <- .query
#     .q_url <- httr::build_url(.rt_url)
#     message(paste0("Request URL: ", .q_url))
#     .resp <- httr::GET(url = .q_url)
#     if (grepl(.format, "CSV", ignore.case = T)) .dat <- readr::read_csv(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
#     if (grepl(.format, "JSON", ignore.case = T)) .dat <- jsonlite::fromJSON(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
#     if (grepl(.format, "CSV|JSON", ignore.case = T)) {
#       .dat <- httr::content(.resp, type = "text/html") %>% rvest::html_node("body") %>% rvest::html_text()  %>% jsonlite::fromJSON()
#       .dat <- as.data.frame(.dat[[3]]) %>% setNames(.dat[[2]]) %>% dplyr::mutate_at(dplyr::vars(Date), ~{lubridate::ymd_hms(., tz = "US/Eastern")})
#     }
#   }
#   return(.dat)
# }

rt_getData <- function(key = NULL, .by = "interval", .i = "hour", .begin = lubridate::today() - lubridate::weeks(12), .end = lubridate::today(), .rk = "overview", .format = "csv", .rt = NULL, .ry = NULL, .group = NULL) {
  # For Debug
  # list(key = keyring::key_get("Rescuetime", "key"), .by = "interval", .i = "minute", .begin = lubridate::ymd("2019-11-16"), .end = lubridate::now(), .rk = "productivity", .format = "CSV", .rt = NULL, .ry = NULL, .group = NULL) %>% list2env(envir = .GlobalEnv)
  # End Debug
  .rt_url <- httr::parse_url("https://www.rescuetime.com/anapi/data")
  # Smart select .i
  if (stringr::str_detect(.i, "^[Mm][Ii]")) .i <-  "minute" else 
    if (stringr::str_detect(.i, "^[Hh]")) .i <- "hour" else
      if (stringr::str_detect(.i, "^[Dd]")) .i <- "day" else
        if (stringr::str_detect(.i, "^[Ww]")) .i <- "week" else
          if (stringr::str_detect(.i, "^[Mm][Oo]")) .i <-  "month"
          # Large by minute requests
          if (.i == "minute" & difftime(lubridate::as_datetime(.end), lubridate::as_datetime(.begin), "days") > 1) {
            # Create intervals of appropriate segments to suit the API restrictions
            
            .segs <- seq.Date(from = .begin, to = .end, by = 1)
            # Sequence the requests
            .dat <- purrr::map(.segs, ~{
              # build query
              .query <- list(key = key,
                             by = .by,
                             interval = .i,
                             restrict_begin = .x,
                             restrict_end = .x,
                             restrict_kind = .rk,
                             restrict_thing = .rt,
                             restring_thingy = .ry,
                             group = .group,
                             format = .format
              ) %>% purrr::compact()
              .rt_url$query <- .query
              .q_url <- httr::build_url(.rt_url)
              message(paste0("Request URL: ", .q_url))
              .resp <- httr::GET(url = .q_url)
              # if (grepl(.format, "CSV|JSON", ignore.case = T)) {
              #   .dat <- httr::content(.resp, type = "text/html") %>% rvest::html_node("body") %>% rvest::html_text()  %>% jsonlite::fromJSON()
              #   .dat <- as.data.frame(.dat[[3]]) %>% setNames(.dat[[2]]) %>% dplyr::mutate_at(dplyr::vars(Date), ~{lubridate::ymd_hms(., tz = "US/Eastern")})
              # }
              if (grepl(.format, "CSV", ignore.case = T)) .dat <- readr::read_csv(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
              if (grepl(.format, "JSON", ignore.case = T)) .dat <- jsonlite::fromJSON(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
              return(.dat)
            })
            .dat <- dplyr::bind_rows(.dat) # Bind the responses
            .dat <- .dat[!duplicated(.dat$Date), ] # Remove duplicates
            .dat <- dplyr::arrange(.dat, Date) # arrange by date
          } else {
            # browser()
            # Normal query
            .query <- list(key = key,
                           perspective = .by,
                           interval = .i,
                           restrict_begin = lubridate::as_date(.begin),
                           restrict_end = lubridate::as_date(.end),
                           restrict_kind = .rk,
                           restrict_thing = .rt,
                           restring_thingy = .ry,
                           group = .group,
                           format = .format
            ) %>% purrr::compact()
            .rt_url$query <- .query
            .q_url <- httr::build_url(.rt_url)
            message(paste0("Request URL: ", .q_url))
            .resp <- httr::GET(url = .q_url)
            if (grepl(.format, "CSV", ignore.case = T)) .dat <- readr::read_csv(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
            if (grepl(.format, "JSON", ignore.case = T)) .dat <- jsonlite::fromJSON(httr::content(.resp, as = "text"), locale = readr::locale(tz = "US/Eastern"))
            # if (grepl(.format, "CSV|JSON", ignore.case = T)) {
            #   .dat <- httr::content(.resp, type = "text/html") %>% rvest::html_node("body") %>% rvest::html_text()  %>% jsonlite::fromJSON()
            #   .dat <- as.data.frame(.dat[[3]]) %>% setNames(.dat[[2]]) %>% dplyr::mutate_at(dplyr::vars(Date), ~{lubridate::ymd_hms(., tz = "US/Eastern")})
            # }
          }
          return(.dat)
}
