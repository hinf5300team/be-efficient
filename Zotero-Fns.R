# ----------------------- Mon Oct 21 07:15:07 2019 ------------------------#
# Zotero extract

#' @title zt_getGroupids
#' Get a list of Zotero Group metadata that includes IDs for further API Calls
#' @param id \code{(char)} Your user id found at [Zotero API Keys](https://www.zotero.org/settings/keys)
#' @return A list with the metadata
#' @details Set the id using \pkg{keyring} with the following: \code{keyring::key_set_with_value(service = "Zotero", username = "id", password = "[YOUR ID]")}
#' @examples 
#' g_ids <- zt_getGroupids() # Provided the id is set via keyring, otherwise key must be supplied as character
#' 
zt_getGroupids <- function(id = keyring::key_get("Zotero", "id")){
  .url <- httr::parse_url("https://api.zotero.org")
  .path <- paste0(c("users", id, "groups"), collapse = "/")
  .query <- list(
    
  )
  .url$path <- .path
  .url <- httr::build_url(.url)
  message(paste0("Query URL: ", .url))
  .resp <- httr::GET(.url, config = httr::config(httr::add_headers(`Zotero-API-Version` = 3)))
  return(httr::content(.resp))
}


# ----------------------- Mon Oct 21 07:49:26 2019 ------------------------#
#' @title zt_getData
zt_getItem <- function(id = NULL, gid = NULL, .item = NULL, .children = F){
  .url <- httr::parse_url("https://api.zotero.org")
  if (!is.null(id)) .path = paste0(c("users", id), collapse = "/") else .path = paste0(c("groups", gid), collapse = "/")
  .path <- paste0(.path, "/items/", .item)
  if (.children) .path <- paste0(.path, "/children")
  .url$path <- .path
  .url <- httr::build_url(.url)
  message(paste0("Query URL: ", .url))
  .resp <- httr::GET(.url, config = httr::config(httr::add_headers(`Zotero-API-Version` = 3)))
  return(httr::content(.resp, type = "application/json"))
}

# ----------------------- Mon Oct 21 07:49:39 2019 ------------------------#
#' @title zt_searchRefs
#' For searching the references
#' @param id The user id if searching a user library
#' @param gid The group id if searching a group library (one or the other is required)
#' @param .format \code{(char)} Defaults to "json" \itemize{
#'  \item \code{atom} will return an Atom feed suitable for use in feed readers or feed-reading libraries.
#'  \item \code{bib}, valid only for item requests, will return a formatted bibliography as XHTML. bib mode is currently limited to a maximum of 150 items.
#'  \item \code{json}, \strong{Default} will return a JSON array for multi-object requests and a single JSON object for single-object requests.
#'  \item \code{keys}, valid for multi-object requests, will return a newline-separated list of object keys. keys mode has no default or maximum limit.
#'  \item \code{versions}, valid for multi-object collection, item, and search requests, will return a JSON object with Zotero object keys as keys and object versions as values. Like keys, versions mode has no default or maximum limit.
#'  \item Export formats, valid only for item requests, will return data in the specified format for each item
#'  }
#' @param .include \code{bib}, \code{citation}, \strong{Default:} \code{data}, export format. Multiple formats can be specified by using a comma as the delimiter (\code{include=data,bib}).
#' \itemize{
#'  \item \code{bib}, valid only for item requests, will return a formatted reference for each item.
#'  \item \code{citation}, valid only for item requests, will return a formatted citation for each item..
#'  \item \code{data}, \strong{Default} will include all writeable fields in JSON format, suitable for modifying and sending back to the API.
#'  \item Export formats, valid only for item requests, will return data in the specified format for each item
#' }
#' @param .qmode Query modes - some apply to items, some to tags
#' For items: \itemize{
#' \item \strong{Default:} \code{titleCreatorYear}, \code{everything} Quick search mode. To include full-text content, use \code{everything}. Searching of other fields will be possible in the future.
#' \item \strong{Default:} \code{contains}, \code{startsWith} Quick search mode. To perform a left-bound search, use \code{startsWith}.
#' }


zt_searchRefs <- function(id = NULL, gid = NULL, .format = "json", .include = "data", .qmode = "everything", .q = NULL, .since = NULL, .tag = NULL){
  .url <- httr::parse_url("https://api.zotero.org")
  if (!is.null(id)) .path = paste0(c("users", id, "items"), collapse = "/") else .path = paste0(c("groups", gid, "items"), collapse = "/")
  .url$path <- .path
  # query
  .query <- list(
    format = .format,
    include = ifelse(length(.include) > 1, paste0(.include, collapse = ","), .include),
    qmode = .qmode,
    q = .q,
    tag = .tag
  ) %>% purrr::compact()
  .url$query <- .query
  .url <- httr::build_url(.url)
  message(paste0("Query URL: ", .url))
  .resp <- httr::GET(.url, config = httr::config(httr::add_headers(`Zotero-API-Version` = 3)))
  return(httr::content(.resp))
}

# 
# 
# keyring::key_set_with_value("rebrandly", username = "key", password = "fa2060c2ac7549ef9ceafa229acb4de1")
# 
# RefManageR::ReadZotero(group = g_ids[[1]]$id, .params = list(tag = "int_content", qmode = "everything"))
